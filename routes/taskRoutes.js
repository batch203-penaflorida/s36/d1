const express = require("express");
// express.router() method allows access to HTTP methods.

const router = express.Router();

const taskControllers = require("../controllers/taskControllers");

console.log(taskControllers);

router.post("/", taskControllers.createTask);

// View All Tasks Route
router.get("/allTasks", taskControllers.getAllTasksController);

// Get a single task
router.get("/getSingleTask/:taskId", taskControllers.getASingleTaskController);

// Update a task status
router.patch("/updateTask/:taskId", taskControllers.updateTaskStatusController);

router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);

module.exports = router;
